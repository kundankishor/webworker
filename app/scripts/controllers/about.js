'use strict';

/**
 * @ngdoc function
 * @name workerwebApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the workerwebApp
 */
angular.module('workerwebApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
