'use strict';

/**
 * @ngdoc function
 * @name workerwebApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the workerwebApp
 */
workerwebApp.controller('MainCtrl', function (Webworker, $http, $scope) {


  var getWorker = new Worker('scripts/controllers/worker.js');
  var postWorker = new Worker('scripts/controllers/worker.js');

  $scope.getRequestCount = 0;
  $scope.postRequestCount = 0;

  getWorker.addEventListener('message', function (e) {
    $scope.getResponse = JSON.parse(e.data);
    $scope.getRequestCount = $scope.getRequestCount + 1;
    $scope.$apply();
  }, false);

  postWorker.addEventListener('message', function (e) {
    $scope.postResponse = JSON.parse(e.data);
    $scope.postRequestCount = $scope.postRequestCount + 1;
    $scope.$apply();
  }, false);

  this.submitGetRequest = function () {
    var httpConfig = {
      url: 'http://fakerestapi.azurewebsites.net/api/Books',
      method: 'GET'
    }
    getWorker.postMessage(httpConfig);
  };

  this.submitPostRequest = function () {
    var httpConfig = {
      url: 'http://fakerestapi.azurewebsites.net/api/Books',
      method: 'POST',
      data: '{"ID":1011,"Title":"Whatever","Description":"Great Book","PageCount":3124,"Excerpt":"Nothing","PublishDate":"2018-01-31T06:02:04.279Z"}'
    }
    postWorker.postMessage(httpConfig);
  };

});
