function fetch(httpConfig, callback) {
  var xhr;
  if (typeof XMLHttpRequest !== 'undefined') {
    xhr = new XMLHttpRequest();
  } else {
    var versions = ['MSXML2.XmlHttp.5.0',
      'MSXML2.XmlHttp.4.0',
      'MSXML2.XmlHttp.3.0',
      'MSXML2.XmlHttp.2.0',
      'Microsoft.XmlHttp'
    ];

    for (var i = 0, len = versions.length; i < len; i++) {
      try {
        xhr = new ActiveXObject(versions[i]);
        break;
      } catch (e) {}
    }
  }


  function ensureReadiness() {
    if (xhr.readyState < 4) {
      return;
    }

    if (xhr.status !== 200) {
      return;
    }

    if (xhr.readyState === 4) {
      callback(xhr);
    }
  }

  xhr.onreadystatechange = ensureReadiness;

  xhr.open(httpConfig.method, httpConfig.url, true);
  xhr.setRequestHeader('Content-type', 'application/json');
  xhr.send(httpConfig.data || '');
}

self.addEventListener('message', function (e) {

  fetch(e.data, function (xhr) {
    var result = xhr.responseText;
    var object = JSON.parse(result);
    self.postMessage(JSON.stringify(object));
  });

}, false);
